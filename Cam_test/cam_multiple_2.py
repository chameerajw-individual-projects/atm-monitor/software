import requests
import serial
import os
import time
import json
import math
from cv2 import (VideoCapture, imshow, waitKey, imwrite, destroyWindow, CAP_DSHOW, CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT)
from datetime import datetime

CAMERA_PORT = 'rtsp://admin:antimatter@123@169.254.60.62:554/Streaming/channels/2/'  # 1 for USB cam. 0 for Lap cam
IMAGE_SAVE_PATH = "D:/Software/Cam_test/Images/"
MAX_CAPTURE_ATTEMPT = 10

def captureImage(path_postfix):
    global IMAGE_SAVE_PATH
    for idx in range(MAX_CAPTURE_ATTEMPT):
        try:
            cam = VideoCapture(CAMERA_PORT)
            #cam.set(CAP_PROP_FRAME_WIDTH, 300)
            #cam.set(CAP_PROP_FRAME_HEIGHT, 200)
            result, image = cam.read()
            if result:
                # imshow("AntiMatterRFID", image)
                now_time = datetime.now()
                current_date_time = now_time.strftime("(%Y-%m-%d) %H-%M-%S")
                # print("Current Time =", current_time)
                image_path = IMAGE_SAVE_PATH + path_postfix
                if not (os.path.exists(image_path)):
                    os.mkdir(image_path)
                #print(image_path)
                png_name = current_date_time + ".png"
                print("png_name =", png_name)
                imwrite(os.path.join(image_path, png_name), image)
                # waitKey(0)
                # destroyWindow("AntiMatterRFID")
                cam.release()
                break
        except:
            print("Error occured while capturing the image")
            pass
        time.sleep(5)  # Sleep 5s before next try
    else:
        print("FAILED TO OPEN CAMERA:", path_postfix)


while (True):
    captureImage("multi_2")
    time.sleep(1)