# program to capture single image from webcam in python

# importing OpenCV library
from cv2 import *

# initialize the camera
# If you have multiple camera connected with
# current device, assign a value in cam_port
# variable according to that
def returnCameraIndexes():
    # checks the first 10 indexes.
    index = 0
    arr = []
    i = 10
    while i > 0:
        cap = VideoCapture(index)
        if cap.read()[0]:
            arr.append(index)
            cap.release()
        index += 1
        i -= 1
    return arr

#print (returnCameraIndexes())

cam_port = 0
cam = VideoCapture(cam_port, CAP_DSHOW)
cam.set(CAP_PROP_FRAME_WIDTH, 300)
cam.set(CAP_PROP_FRAME_HEIGHT, 200)

width = cam.get(CAP_PROP_FRAME_WIDTH)
height = cam.get(CAP_PROP_FRAME_HEIGHT)
print(width, height)

# reading the input using the camera
result, image = cam.read()

# If image will detected without any error,
# show result
if result:

    # showing result, it take frame name and image
    # output
    imshow("GeeksForGeeks", image)

    # saving image in local storage
    imwrite("GeeksForGeeks.png", image)

    # If keyboard interrupt occurs, destroy image
    # window
    waitKey(0)
    destroyWindow("GeeksForGeeks")

# If captured image is corrupted, moving to else part
else:
    print("No image detected. Please! try again")