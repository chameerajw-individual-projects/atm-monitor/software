import requests
import serial
import os
import time
import json
import math
#from cv2 import *
from cv2 import (VideoCapture, imshow, waitKey, imwrite, destroyWindow, CAP_DSHOW, CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT)
from datetime import datetime
import base64
import random

POST_URL = 'https://dashboard.antimatter.lk/device/upload.php'
MAX_DATA_SEND_ATTEMPT = 3
API = '154A45'
DEVICE_ID_1 = 'NTB-0001'
DEVICE_ID_2 = 'NTB-0002'
DEVICE_ID_3 = 'NTB-0003'

data_json_1 = {'API': API,
             'd_id': DEVICE_ID_1,
             'room_temp_front': 37.08,
             'f_door_state': 0,
             'room_temp_back': 26.49,
             'room_state': 'Normal',
             'b_door_state': 0,
             'b_door_count': 0,
             'l1': 1,
             'l2': 1,
             'l3': 1,
             'l4': 1,
             'l_state': 'Normal',
             'grid': 1,
             'trip': 1,
             'ups': 1,
             'power_state': 'Normal',
             'power_desc': 'G: 1, T: 1, U: 1',
             'atm_temp': 24.57,
             'atm_vibr': 0,
             'atm_state': 'Normal',
             'atm_img': 'NULL',
             'unusual_state': 0,
             'unusual_img': 'NULL',
             'device_state': 'Normal'}

data_json_2 = {'API': API,
             'd_id': DEVICE_ID_2,
             'room_temp_front': 35.79,
             'f_door_state': 0,
             'room_temp_back': 26.65,
             'room_state': 'Normal',
             'b_door_state': 0,
             'b_door_count': 0,
             'l1': 1,
             'l2': 1,
             'l3': 1,
             'l4': 1,
             'l_state': 'Normal',
             'grid': 1,
             'trip': 1,
             'ups': 1,
             'power_state': 'Normal',
             'power_desc': 'G: 1, T: 1, U: 1',
             'atm_temp': 25.98,
             'atm_vibr': 0,
             'atm_state': 'Normal',
             'atm_img': 'NULL',
             'unusual_state': 0,
             'unusual_img': 'NULL',
             'device_state': 'Normal'}

data_json_3 = {'API': API,
             'd_id': DEVICE_ID_3,
             'room_temp_front': 39.58,
             'f_door_state': 0,
             'room_temp_back': 27.36,
             'room_state': 'Normal',
             'b_door_state': 0,
             'b_door_count': 0,
             'l1': 1,
             'l2': 1,
             'l3': 1,
             'l4': 1,
             'l_state': 'Normal',
             'grid': 1,
             'trip': 1,
             'ups': 1,
             'power_state': 'Normal',
             'power_desc': 'G: 1, T: 1, U: 1',
             'atm_temp': 25.56,
             'atm_vibr': 0,
             'atm_state': 'Normal',
             'atm_img': 'NULL',
             'unusual_state': 0,
             'unusual_img': 'NULL',
             'device_state': 'Normal'}

def UpdateJson_1():
    try:
        global data_json_1
        now_time = datetime.now()
        if(now_time.hour < 6):
            rand_val = float(random.randint(0, 60)) / 1000.0
            data_json_1['room_temp_front'] = round(data_json_1['room_temp_front'] - rand_val, 2)
            rand_val = float(random.randint(0, 60)) / 1000.0
            data_json_1['room_temp_back'] = round(data_json_1['room_temp_back'] - rand_val, 2)
            rand_val = float(random.randint(0, 60)) / 1000.0
            data_json_1['atm_temp'] = round(data_json_1['atm_temp'] - rand_val, 2)
            data_json_1['l1'] = 1
            data_json_1['l2'] = 1
            data_json_1['l3'] = 1
            data_json_1['l4'] = 1
        else:
            rand_val = float(random.randint(0, 170)) / 1000.0
            data_json_1['room_temp_front'] = round(data_json_1['room_temp_front'] + rand_val, 2)
            rand_val = float(random.randint(0, 170)) / 1000.0
            data_json_1['room_temp_back'] = round(data_json_1['room_temp_back'] + rand_val, 2)
            rand_val = float(random.randint(0, 170)) / 1000.0
            data_json_1['atm_temp'] = round(data_json_1['atm_temp'] + rand_val, 2)
            data_json_1['l1'] = 0
            data_json_1['l2'] = 0
            data_json_1['l3'] = 0
            data_json_1['l4'] = 0
    except:
        pass

def UpdateJson_2():
    try:
        global data_json_2
        now_time = datetime.now()
        if(now_time.hour < 6):
            rand_val = float(random.randint(0, 60)) / 1000.0
            data_json_2['room_temp_front'] = round(data_json_2['room_temp_front'] - rand_val, 2)
            rand_val = float(random.randint(0, 60)) / 1000.0
            data_json_2['room_temp_back'] = round(data_json_2['room_temp_back'] - rand_val, 2)
            rand_val = float(random.randint(0, 60)) / 1000.0
            data_json_2['atm_temp'] = round(data_json_2['atm_temp'] - rand_val, 2)
            data_json_2['l1'] = 1
            data_json_2['l2'] = 1
            data_json_2['l3'] = 1
            data_json_2['l4'] = 1
        else:
            rand_val = float(random.randint(0, 170)) / 1000.0
            data_json_2['room_temp_front'] = round(data_json_2['room_temp_front'] + rand_val, 2)
            rand_val = float(random.randint(0, 170)) / 1000.0
            data_json_2['room_temp_back'] = round(data_json_2['room_temp_back'] + rand_val, 2)
            rand_val = float(random.randint(0, 170)) / 1000.0
            data_json_2['atm_temp'] = round(data_json_2['atm_temp'] + rand_val, 2)
            data_json_2['l1'] = 0
            data_json_2['l2'] = 0
            data_json_2['l3'] = 0
            data_json_2['l4'] = 0
    except:
        pass
    
def UpdateJson_3():
    try:
        global data_json_3
        now_time = datetime.now()
        if(now_time.hour < 6):
            rand_val = float(random.randint(0, 60)) / 1000.0
            data_json_3['room_temp_front'] = round(data_json_3['room_temp_front'] - rand_val, 2)
            rand_val = float(random.randint(0, 60)) / 1000.0
            data_json_3['room_temp_back'] = round(data_json_3['room_temp_back'] - rand_val, 2)
            rand_val = float(random.randint(0, 60)) / 1000.0
            data_json_3['atm_temp'] = round(data_json_3['atm_temp'] - rand_val, 2)
            data_json_3['l1'] = 1
            data_json_3['l2'] = 1
            data_json_3['l3'] = 1
            data_json_3['l4'] = 1
        else:
            rand_val = float(random.randint(0, 170)) / 1000.0
            data_json_3['room_temp_front'] = round(data_json_3['room_temp_front'] + rand_val, 2)
            rand_val = float(random.randint(0, 170)) / 1000.0
            data_json_3['room_temp_back'] = round(data_json_3['room_temp_back'] + rand_val, 2)
            rand_val = float(random.randint(0, 170)) / 1000.0
            data_json_3['atm_temp'] = round(data_json_3['atm_temp'] + rand_val, 2)
            data_json_3['l1'] = 0
            data_json_3['l2'] = 0
            data_json_3['l3'] = 0
            data_json_3['l4'] = 0
    except:
        pass

while(True):
    try:
        UpdateJson_1()
        try:
            print("data_json_1: ", data_json_1)
            for i in range(MAX_DATA_SEND_ATTEMPT):
                request_send = requests.post(POST_URL, json=data_json_1)
                if (int(request_send.status_code) == 200):
                    time.sleep(30)
                    break
                else:
                    pass
        except:
            pass

        UpdateJson_2()
        try:
            print("data_json_2: ", data_json_2)
            for i in range(MAX_DATA_SEND_ATTEMPT):
                request_send = requests.post(POST_URL, json=data_json_2)
                if (int(request_send.status_code) == 200):
                    time.sleep(30)
                    break
                else:
                    pass
        except:
            pass

        UpdateJson_3()
        try:
            print("data_json_3: ", data_json_3)
            for i in range(MAX_DATA_SEND_ATTEMPT):
                request_send = requests.post(POST_URL, json=data_json_3)
                if (int(request_send.status_code) == 200):
                    time.sleep(30)
                    break
                else:
                    pass
        except:
            pass

        time.sleep(random.randint(0, 10))
    except:
        pass