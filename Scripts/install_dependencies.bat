@ECHO OFF
ECHO Installing Required Dependencies
ECHO ********************************
ECHO 

ECHO UPDATING PIP
python.exe -m pip install --upgrade pip
ECHO ********************************
ECHO 

ECHO INSTALLING pyserial
pip install pyserial
ECHO ********************************
ECHO 

ECHO INSTALLING pyyaml
pip install pyyaml
ECHO ********************************
ECHO 

ECHO INSTALLING requests
pip install requests
ECHO ********************************
ECHO 

ECHO INSTALLING opencv-python
pip install opencv-python
ECHO ********************************
ECHO 

ECHO INSTALLING opencv-contrib-python
pip install opencv-contrib-python
ECHO ********************************
ECHO 

ECHO INSTALLING tensorflow
pip install tensorflow
ECHO ********************************
ECHO 

ECHO INSTALLING pillow
pip install pillow
ECHO ********************************
ECHO 

ECHO INSTALLING playsound==1.2.2
pip install playsound==1.2.2
ECHO ********************************
ECHO 

ECHO INSTALLING ultralytics
pip install ultralytics
ECHO ********************************
ECHO 

ECHO FINISHED INSTALLING
#PAUSE