@ECHO OFF

call git_pull.bat
call clasher.bat
call install_dependencies.bat

ECHO CREATING FOLDER
if not exist "../Sensor_reader/Images/Person_In" mkdir "../Sensor_reader/Images/Person_In"
if not exist "../Sensor_reader/Images/Person_In/Checked" mkdir "../Sensor_reader/Images/Person_In/Checked"
if not exist "../Sensor_reader/Images/Overtime" mkdir "../Sensor_reader/Images/Overtime"
if not exist "../Sensor_reader/Images/Suspicious" mkdir "../Sensor_reader/Images/Suspicious"

if not exist "../ManualButtons/Images" mkdir "../ManualButtons/Images"

if not exist "../RFID_reader/Images" mkdir "../RFID_reader/Images"
ECHO 

ECHO STARTING SENSOR READER
cd "..\Sensor_reader\"
start Sensor_reader.bat
ECHO 

ECHO STARTING MANUAL BUTTONS
cd "..\ManualButtons\"
start ManualButtons.bat
ECHO 

ECHO STARTING HELMET DET
cd "..\HelmetDet\"
start infer.bat
ECHO 

ECHO STARTING RFID READER
cd "..\RFID_reader\"
start RFID_reader.bat
ECHO 

#ECHO STARTING HUMAN DETECTION
#cd "..\HumanDetection\"
#start infer.bat
#ECHO 