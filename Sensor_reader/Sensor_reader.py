import requests
import serial
import os
import shutil
import time
import json
import math
#from cv2 import *
from cv2 import (VideoCapture, imshow, waitKey, imencode, imwrite, imread, pyrDown, destroyWindow, CAP_DSHOW, CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT)
from datetime import datetime
import base64
import yaml

BASE_CONF_FILE_PATH = "./../base_config.yaml"
NEW_IMAGE_PATH = "./Images/Overtime/"

try:
    with open(BASE_CONF_FILE_PATH, "r") as base_cfg_file:
        base_cfg = yaml.safe_load(base_cfg_file)

    COM_PORT = str(base_cfg["SENSOR_COM_PORT"])
    DEVICE_ID = str(base_cfg["DEVICE_ID"])

except:
    print("An exception occured at loading BASE CONFIG")

# CAMERA_PORT = 0  # 1 for USB cam. 0 for Lap cam
CAMERA_PORT = 'rtsp://admin:antimatter@123@169.254.60.62:554/Streaming/channels/1/'  # /1 for better quality
IMAGE_SAVE_PATH = "./Images/"
MAX_CAPTURE_ATTEMPT = 10
CAM_RESOLUTION_HEIGHT = 200
CAM_RESOLUTION_WIDTH = 300
PERSON_IN_POSTFIX = "Person_In"
OVERTIME_POSTFIX = "Overtime"
SUSPICIOUS_POSTFIX = "Suspicious"

DATASET_SAVE_PATH = "./dataset.json"

POST_URL = 'https://dashboard.antimatter.lk/device/upload.php'
MAX_DATA_SEND_ATTEMPT = 3
API = '154A45'

MAX_IDLE_DURATION = 3  # device should send a packet atleast once within three minutes

SENSOR_LOG_FILE_PATH = "./sensor_log.txt"

BACK_ROOM_TEMP_LOWER_THRESHOLD = 15.00
BACK_ROOM_TEMP_HIGHER_THRESHOLD = 45.00

ATM_TEMP_LOWER_THRESHOLD = 15.0  # 15.00
ATM_TEMP_HIGHER_THRESHOLD = 40.0  #25.00

LIGHT_OFF_HOUR = 6
LIGHT_ON_HOUR = 18

ENCODING = 'utf-8'

MAXIMUM_ALLOWED_DURATION = 15  # 15  # maximum allowed duration in minutes

while(True):
    try:
        serialPort = serial.Serial(_COM_PORT, baudrate=115200, timeout=180)  # send every 1 min #*********************need to update to 180
        break
    except Exception as e:
        print(e)
        print("FAILED TO OPEN COM-PORT:", COM_PORT)
    time.sleep(1)  # Retry to open serial port every 5s

savedSet = set()
try:
    for file in os.listdir(NEW_IMAGE_PATH):
        fullpath = os.path.join(NEW_IMAGE_PATH, file)
        if os.path.isfile(fullpath):
            savedSet.add(file)
    log_message = "Initial File count: " + str(len(savedSet))
    print(log_message)
except Exception as e:
    print(e)
    print("FAILED TO READ INITIAL FILES")

old_read_string = ""  # save previous string to compare with the new one

is_reset_door_count = False
yesterday_door_count = 0

last_sent_time = datetime.now()

data_json = {'API': API,
             'd_id': DEVICE_ID,
             'room_temp_front': 25.00,
             'f_door_state': 0,
             'room_temp_back': 25.00,
             'room_state': 'Normal',
             'b_door_state': 0,
             'b_door_count': 0,
             'l1': 0,
             'l2': 0,
             'l3': 0,
             'l4': 0,
             'l_state': 'Normal',
             'grid': 1,
             'trip': 1,
             'ups': 1,
             'power_state': 'Normal',
             'power_desc': 'empty',
             'atm_temp': 25.00,
             'atm_vibr': 0,
             'atm_state': 'Normal',
             'atm_img': 'NULL',
             'unusual_state': 0,
             'unusual_img': 'NULL',
             'device_state': 'Normal'}


def captureImage(path_postfix):
    global IMAGE_SAVE_PATH
    for idx in range(MAX_CAPTURE_ATTEMPT):
        try:
            # cam = VideoCapture(CAMERA_PORT, CAP_DSHOW)
            cam = VideoCapture(CAMERA_PORT)
            # cam.set(CAP_PROP_FRAME_WIDTH, CAM_RESOLUTION_WIDTH)
            # cam.set(CAP_PROP_FRAME_HEIGHT, CAM_RESOLUTION_HEIGHT)
            result, image = cam.read()
            if result:
                # imshow("AntiMatterRFID", image)
                now_time = datetime.now()
                current_date_time = now_time.strftime("(%Y-%m-%d) %H-%M-%S")
                #print("Current Time =", current_time)
                image_path = IMAGE_SAVE_PATH + path_postfix
                if not(os.path.exists(image_path)):
                    os.mkdir(image_path)
                png_name = current_date_time + ".png"
                log_message = 'Image name: ' + png_name
                print(log_message)
                updateLog(log_message)

                imwrite(os.path.join(image_path, png_name), image)
                # waitKey(0)
                # destroyWindow("AntiMatterRFID")
                cam.release()

                converted_image = pyrDown(image)
                _, im_arr = imencode('.png', converted_image)
                im_bytes = im_arr.tobytes()

                base64_bytes = base64.b64encode(im_bytes)
                base64_string = base64_bytes.decode(ENCODING)
                return "data:image/png;base64," + base64_string
                # return "NULL"
            else:
                log_message = "No Image"
                print(log_message)
                updateLog(log_message)
        except:
            print("Error occured while capturing the image")
            pass
        time.sleep(5)  # Sleep 5s before next try
    return 'NULL'

def getSerialDic(in_string):
    try:
        if (len(in_string) > 5):
            start_idx = in_string.index("{")
            end_idx = in_string.index("}")
            trim_string = in_string[start_idx: end_idx + 1]
            #print('trim_string: ', trim_string)
            return json.loads(trim_string)
        else:
            return {}
    except:
        print("Failed inside getSerialDic Function")
    return {}

def resetDoorCount():
    try:
        global is_reset_door_count
        global yesterday_door_count
        now_time = datetime.now()
        if(now_time.hour == 0):  # midnight. type(now_time.hour) is int
            if(is_reset_door_count == False):  # door count still not reset
                try:
                    yesterday_door_count = int(data_json['b_door_count'])
                    data_json['b_door_count'] = 0
                    request_to_reset_b_door_count = requests.post(POST_URL, json=data_json)
                    print("Request request_to_reset_b_door_count response:", request_to_reset_b_door_count.status_code)
                    is_reset_door_count = True
                except:
                    print("Cannot cast b_door_count to int:", data_json['b_door_count'])
                    pass
        else:
            is_reset_door_count = False  # reset door count reset variable at other hours
    except:
        print("Failed inside resetDoorCount Function")
        pass

def checkAndUpdateOvertime():
    global savedSet
    global data_json
    try:
        retrievedSet = set()
        for file in os.listdir(NEW_IMAGE_PATH):
            fullpath = os.path.join(NEW_IMAGE_PATH, file)
            if os.path.isfile(fullpath):
                retrievedSet.add(file)

        newSet = retrievedSet - savedSet
        if (len(newSet) > 0):
            image_path_full = os.path.join(NEW_IMAGE_PATH, list(newSet)[0])
            image = imread(image_path_full)
            converted_image = pyrDown(image)
            _, im_arr = imencode('.png', converted_image)
            im_bytes = im_arr.tobytes()

            base64_bytes = base64.b64encode(im_bytes)
            base64_string = base64_bytes.decode(ENCODING)

            print("Setting unusual state")
            data_json['unusual_state'] = 1
            data_json['unusual_img'] = "data:image/png;base64," + base64_string

            savedSet = retrievedSet
        else:
            print("ReSetting unusual state")
            data_json['unusual_state'] = 0
            data_json['unusual_img'] = 'NULL'

    except Exception as e:
        print("Failed inside checkAndUpdateOvertime Function" + str(e))
        pass


def updateDataJson(in_dict):
    try:
        global data_json
        data_json['device_state'] = 'Abnormal'  # set initial device state to abnormal. make it normal if the except not triggered

        data_json['atm_img'] = 'NULL'  # reset to default. below code will update if we need to send an image
        data_json['unusual_state'] = 0  # reset to default. below code will update if needed
        data_json['unusual_img'] = 'NULL'  # reset to default. below code will update if we need to send an image

        if('pm' in in_dict.keys()):  # person movement detected from Sonar
            try:
                if(int(in_dict['pm']) == 1):  # person just came. take a photo and run image processing
                    # take an image and save
                    captureImage(PERSON_IN_POSTFIX)
                    pass
                else:  # person moved out
                    pass
            except:
                print("Cannot cast person-movement to int (initial attempt):", in_dict['pm'])
                pass

        if('rtf' in in_dict.keys()):  # room_temp_front
            data_json['room_temp_front'] = in_dict['rtf']

        if('fds' in in_dict.keys()):  # front_door_state
            try:
                current_fds = int(in_dict['fds'])
                previous_fds = int(data_json['f_door_state'])
                if((current_fds == 1) and (previous_fds == 0)):  # person just came. take a photo and run image processing
                    # take an image and save
                    time.sleep(2)  # Wait for person to completely ome-in or go-out
                    captureImage(PERSON_IN_POSTFIX)
                    pass
                else:  # person moved out
                    pass
            except:
                print("Cannot cast f_door_state to int:", in_dict['fds'])
                pass
            data_json['f_door_state'] = in_dict['fds']

        if('rtb' in in_dict.keys()):  # room_temp_back
            data_json['room_temp_back'] = in_dict['rtb']
            try:
                read_back_room_temp = float(in_dict['rtb'])

                if((read_back_room_temp <= BACK_ROOM_TEMP_LOWER_THRESHOLD) or (read_back_room_temp >= BACK_ROOM_TEMP_HIGHER_THRESHOLD)):
                    data_json['room_state'] = 'Abnormal'
                else:
                    data_json['room_state'] = 'Normal'
            except:
                print("Cannot cast rtb to float:", in_dict['rtb'])
                pass

        if('bds' in in_dict.keys()):  # back_door_state
            data_json['b_door_state'] = in_dict['bds']

        if('bdc' in in_dict.keys()):  # back_door_count
            try:
                today_b_door_count = int(in_dict['bdc']) - yesterday_door_count
                data_json['b_door_count'] = today_b_door_count
            except:
                print("Cannot cast bdc to int:", in_dict['rtb'])
                pass

        if ('bds' in in_dict.keys()):  # back_door_state
            data_json['b_door_state'] = in_dict['bds']

        if(('l1' in in_dict.keys()) and ('l2' in in_dict.keys()) and ('l3' in in_dict.keys()) and ('l4' in in_dict.keys())):  # light sensor data present
            data_json['l1'] = in_dict['l1']
            data_json['l2'] = in_dict['l2']
            data_json['l3'] = in_dict['l3']
            data_json['l4'] = in_dict['l4']

        light_state = 'Abnormal'
        now_time = datetime.now()
        if((now_time.hour >= LIGHT_OFF_HOUR) and (now_time.hour < LIGHT_ON_HOUR)):  # 06:00 to 18:00. all lights should be OFF. type(now_time.hour) is int
            try:
                if((int(data_json['l1'] == 1)) or (int(data_json['l2'] == 1)) or (int(data_json['l3'] == 1)) or (int(data_json['l4'] == 1))):  # if a light is on: abnormal
                    light_state = 'Abnormal'
                else:
                    light_state = 'Normal'
            except:
                light_state = 'Unknown'
                print("Cannot compare in DAY l1,l2,l3,l4 int: ", data_json['l1'], data_json['l2'], data_json['l3'], data_json['l4'], sep=', ')
                pass
        else:  # 18:00 to next day 06:00. all lights should be ON
            try:
                if((int(data_json['l1'] == 0)) or (int(data_json['l2'] == 0)) or (int(data_json['l3'] == 0)) or (int(data_json['l4'] == 0))):  # if a light is off: abnormal
                    light_state = 'Abnormal'
                else:
                    light_state = 'Normal'
            except:
                light_state = 'Unknown'
                print("Cannot compare in NIGHT l1,l2,l3,l4 int: ", data_json['l1'], data_json['l2'], data_json['l3'], data_json['l4'], sep=', ')
                pass
        if(light_state == 'Normal'):
            # get the response from server
            server_light_state = 'Normal'
            if(server_light_state == 'Normal'):
                data_json['l_state'] = 'Normal'
            else:  # server side l_state is abnormal. DO NOT correct as normal. instead update data_json
                data_json['l_state'] = 'Abnormal'
        elif(light_state == 'Abnormal'): # server_light_state is Abnormal. update data_json
            data_json['l_state'] = 'Abnormal'
        else:  # if light_state is unknown: pass
            pass

        if(('grid' in in_dict.keys()) and ('trip' in in_dict.keys()) and ('ups' in in_dict.keys())):  # power data present
            data_json['grid'] = in_dict['grid']
            data_json['trip'] = in_dict['trip']
            data_json['ups'] = in_dict['ups']

        try:
            if(int(data_json['ups']) == 1):  # UPS is up. device is powered
                data_json['power_state'] = 'Normal'
            else:  # UPS is DOWN. device is not powered
                data_json['power_state'] = 'Abnormal'
        except:
            print("Cannot compare ups in int:", data_json['ups'])
            pass
        try:
            power_desc_string = 'G: ' + str(data_json['grid']) + ', T: ' + str(data_json['trip']) + ', U: ' + str(data_json['ups'])
            data_json['power_desc'] = power_desc_string
        except:
            print("Cannot cast power_desc to str: ", data_json['grid'], data_json['trip'], data_json['ups'], sep=', ')
            pass

        if('atmt' in in_dict.keys()):  # atm_temp data available
            data_json['atm_temp'] = in_dict['atmt']
            try:
                read_atm_temp = float(in_dict['atmt'])
                if((read_atm_temp <= ATM_TEMP_LOWER_THRESHOLD) or (read_atm_temp >= ATM_TEMP_HIGHER_THRESHOLD)):  # atm_temp is abnormal
                    data_json['atm_state'] = 'Abnormal'
                    # take an image. save and send
                    ### data_json['atm_img'] = captureImage(SUSPICIOUS_POSTFIX) #uncomment to send
                    # data_json['atm_img'] = 'IMAGE DATA in base64'
                else:  # atm_temp is in range
                    data_json['atm_state'] = 'Normal'
                    data_json['atm_img'] = 'NULL'
            except:
                print("Cannot cast atmt to float:", in_dict['atmt'])
                pass

        if('atmv' in in_dict.keys()):  # atm_vibr data available
            data_json['atm_vibr'] = in_dict['atmv']  # need to send it as 'Normal'
            try:
                read_atm_vibr = int(in_dict['atmv'])
                if(in_dict['atmv'] == 1):  # atm_vibr detected
                    # data_json['atm_vibr'] = 'Abnormal'
                    if(data_json['atm_state'] == 'Normal'):  # no temperature abnormality detected. only the vibration. need to send image
                        data_json['atm_state'] = 'Abnormal'
                        # take an image. save and send
                        ### data_json['atm_img'] = captureImage(SUSPICIOUS_POSTFIX) #uncomment to send
                        # data_json['atm_img'] = 'IMAGE DATA in base64'
                    else:  # atm_vibr detected. atm_state is abnormal. already temp has captured an image
                        #nothing to do
                        pass
                else:  # no atm_vibr detected
                    # data_json['atm_vibr'] = 'Normal'
                    pass
            except:
                print("Cannot cast atmv to int:", in_dict['atmv'])
                pass

        if('it' in in_dict.keys()):  # in-time detected
            try:
                read_in_time = int(in_dict['it'])
                if(read_in_time >= MAXIMUM_ALLOWED_DURATION):  # someone is staying than usual time
                    read_person_movement = 1  # assume person is still inside
                    if('pm' in in_dict.keys()):  # person-movement data is available
                        try:
                            read_person_movement = int(in_dict['pm'])  # if person is gone and in-time received, no need to capture
                        except:
                            print("Cannot cast person-movement to int:", in_dict['pm'])
                            pass
                    if(read_person_movement == 1):  # person is still inside
                        data_json['unusual_state'] = 1
                        # take an image. save and send
                        data_json['unusual_img'] = captureImage(OVERTIME_POSTFIX)
                        # data_json['unusual_img'] = 'IMAGE DATA in base64'
                    else:  # time is passed. but the person is also gone
                        data_json['unusual_state'] = 0
                        data_json['unusual_img'] = 'NULL'
            except:
                print("Cannot cast in-time to int:", in_dict['it'])
                pass

        checkAndUpdateOvertime()  # Check for Overtime or unusual_state s

        data_json['device_state'] = 'Normal'  # successfully updated data_json

    except:
        data_json['device_state'] = 'Abnormal'
        print("Failed inside updateDataJson Function")
        pass

def updateLog(write_string):
    try:
        with open(SENSOR_LOG_FILE_PATH, "a") as file_object:
            # Append write_string + \n at the end of file
            file_object.write(write_string + '\n')
    except:
        print("Failed inside updateLog Function")
        pass

try:
    if(os.path.isfile(DATASET_SAVE_PATH)):
        with open(DATASET_SAVE_PATH, 'r') as openfile:
            json_object = json.load(openfile)
            data_json = json_object
    else:
        json_object = json.dumps(data_json, indent=1)
        with open(DATASET_SAVE_PATH, "w") as outfile:
            outfile.write(json_object)
except:
    print("Failed to load JSON file in the begining")

while (True):
    try:
        read_string = ""
        resetDoorCount()

        try:
            read_string = str(serialPort.readline())  # convert the byte to string

        except:
            log_message = "Serial Port Disconnected"
            print(log_message)
            updateLog(log_message)
            try:
                serialPort.close()
                time.sleep(5)
                serialPort = serial.Serial(COM_PORT, baudrate=115200, timeout=180)  # send every 1 min #*********************need to update to 180= serial.Serial(COM_PORT, baudrate=115200, timeout=180)  # send every 1 min #*********************need to update to 180
                time.sleep(5)  # If successfully reopened- Wait the loop for 5s
            except:
                log_message = "Failed to Re-Open Serial Port"
                print(log_message)
                updateLog(log_message)
                continue

        read_string = read_string.replace('?', '')  # replace if there are any unwanted values
        read_string = read_string.replace('!', '')  # replace if there are any unwanted values
        read_string = read_string.replace('%', '')  # replace if there are any unwanted values
        if (read_string == old_read_string):  # if duplicate string received: continue
            #print("Same as old")
            continue

        old_read_string = read_string  # new string received

        log_message = 'received_string: ' + str(read_string)
        print(log_message)
        updateLog(log_message)

        read_dictionary = getSerialDic(read_string)  # get the cast dictionary

        if(len(read_dictionary.keys()) == 0):  # empty dictionary received
            # Invalid dictionary received from the device or serial timeout occurred
            time_difference = datetime.now() - last_sent_time
            time_difference_minutes = time_difference.total_seconds() / 60
            if(time_difference_minutes >= MAX_IDLE_DURATION):
                # serial timeout occurred, need to send the packet anyway.
                log_message = "Time Difference in Minutes: " + str(time_difference_minutes)
                print(log_message)
                updateLog(log_message)
                checkAndUpdateOvertime()
                pass
            else:
                continue  # Invalid dictionary received. terminate
        else:
            # Valid dictionary received.
            updateDataJson(read_dictionary)
            # log_message = "Updated data_json: " + str(data_json)
            log_message = "Updated data_json to send"
            print(log_message)
            updateLog(log_message)

            # Save updated json
            json_object = json.dumps(data_json, indent=1)
            with open(DATASET_SAVE_PATH, "w") as outfile:
                outfile.write(json_object)

        now = datetime.now()
        current_date_time = now.strftime("(%Y-%m-%d) %H-%M-%S")
        log_message = "Sending the request @ = " + str(current_date_time)
        print(log_message)
        updateLog(log_message)

        try:
            for i in range(MAX_DATA_SEND_ATTEMPT):
                request_send = requests.post(POST_URL, json=data_json)
                if(int(request_send.status_code) == 200):  # send failed
                    last_sent_time = datetime.now()  # successfully sent
                    print(request_send.text)
                    #print("Request response:", request_send.status_code)
                    #print(request_send.text)
                    break
                else:
                    log_message = "Request response:" + str(request_send.status_code)
                    print(log_message)
                    updateLog(log_message)
            else:  # if break is not occured
                log_message = "Failed to send request for (times):" + str(MAX_DATA_SEND_ATTEMPT)
                print(log_message)
                updateLog(log_message)
        except:
            log_message = "Exception occurred send request_send"
            print(log_message)
            updateLog(log_message)
            pass

        log_message = "***********************************************************************************"  # Line terminator
        print(log_message)
        updateLog(log_message)

    except Exception as e:
        print(e)
        log_message = "Detected an error in Main Loop"
        print(log_message)
        updateLog(log_message)
        pass
