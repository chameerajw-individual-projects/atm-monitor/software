
import requests
import serial
import os
import shutil
import time
import json
import math
#from cv2 import *
from cv2 import (VideoCapture, imshow, waitKey, imencode, imwrite, imread, pyrDown, destroyWindow, CAP_DSHOW, CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT)
from datetime import datetime
import base64
import yaml

BASE_CONF_FILE_PATH = "./../base_config.yaml"
NEW_IMAGE_PATH = "./Images/Overtime/"

savedSet = set()
ENCODING = 'utf-8'

try:
    for file in os.listdir(NEW_IMAGE_PATH):
        fullpath = os.path.join(NEW_IMAGE_PATH, file)
        if os.path.isfile(fullpath):
            savedSet.add(file)
    log_message = "Initial File count: " + str(len(savedSet))
    print(log_message)
except Exception as e:
    print(e)
    print("FAILED TO READ INITIAL FILES")

data_json = {'room_temp_front': 25.00,
             'f_door_state': 0,
             'room_temp_back': 25.00,
             'room_state': 'Normal',
             'b_door_state': 0,
             'b_door_count': 0,
             'l1': 0,
             'l2': 0,
             'l3': 0,
             'l4': 0,
             'l_state': 'Normal',
             'grid': 1,
             'trip': 1,
             'ups': 1,
             'power_state': 'Normal',
             'power_desc': 'empty',
             'atm_temp': 25.00,
             'atm_vibr': 0,
             'atm_state': 'Normal',
             'atm_img': 'NULL',
             'unusual_state': 0,
             'unusual_img': 'NULL',
             'device_state': 'Normal'}

def checkAndUpdateOvertime():
    global savedSet
    global data_json
    try:
        retrievedSet = set()
        for file in os.listdir(NEW_IMAGE_PATH):
            fullpath = os.path.join(NEW_IMAGE_PATH, file)
            if os.path.isfile(fullpath):
                retrievedSet.add(file)

        newSet = retrievedSet - savedSet
        if (len(newSet) > 0):
            image_path_full = os.path.join(NEW_IMAGE_PATH, list(newSet)[0])
            image = imread(image_path_full)
            converted_image = pyrDown(image)
            _, im_arr = imencode('.png', converted_image)
            im_bytes = im_arr.tobytes()

            base64_bytes = base64.b64encode(im_bytes)
            base64_string = base64_bytes.decode(ENCODING)

            data_json['unusual_state'] = 1
            data_json['unusual_img'] = base64_string

            savedSet = retrievedSet
        else:
            data_json['unusual_state'] = 0
            data_json['unusual_img'] = 'NULL'

    except Exception as e:
        print("Failed inside checkAndUpdateOvertime Function" + str(e))
        pass

while(True):
    checkAndUpdateOvertime()
    time.sleep(5)