import requests
import serial
import os
import time
import json
import cv2
import math
#from cv2 import *
from cv2 import (VideoCapture, imshow, waitKey, imwrite, destroyWindow, CAP_DSHOW, CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT)
from datetime import datetime
import base64
from PIL import Image

path_temp = "./Images/Person_In/"
helmet = "./Images/Person_In/With_helmet (16) - Copy.png"
no_helmet = "./Images/Person_In/(2022-08-21) 15-16-56 - Copy.png"
while (True):
    if os.path.exists(path_temp + "temp_helmet.png"):
        os.remove(path_temp + "temp_helmet.png")
    if os.path.exists(path_temp + "temp_no_helmet.png"):
        os.remove(path_temp + "temp_no_helmet.png")

    time.sleep(5)  # Retry to open serial port every 5s

    img = cv2.imread(helmet)
    imwrite(os.path.join(path_temp, "temp_helmet.png"), img)
    time.sleep(5)  # Retry to open serial port every 5s

    img2 = cv2.imread(no_helmet)
    imwrite(os.path.join(path_temp, "temp_no_helmet.png"), img2)
    break