import os
import time
import requests
import json
from json import dumps

from datetime import datetime
import base64
from cv2 import (VideoCapture, imread, imencode, imshow, waitKey, pyrDown, imwrite, destroyWindow, CAP_DSHOW, CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT)
import yaml
from playsound import playsound  # playsound==1.2.2
import numpy as np
import yaml

BASE_CONF_FILE_PATH = "./../base_config.yaml"

try:
    with open(BASE_CONF_FILE_PATH, "r") as base_cfg_file:
        base_cfg = yaml.safe_load(base_cfg_file)

    DEVICE_ID = str(base_cfg["DEVICE_ID"])

except:
    print("An exception occured at loading BASE CONFIG")


CAMERA_PORT = 'rtsp://admin:antimatter@123@169.254.60.62:554/Streaming/channels/1/'  # /1 for better quality
# CAMERA_PORT = 1
MAX_CAPTURE_ATTEMPT = 10
CAM_RESOLUTION_HEIGHT = 200
CAM_RESOLUTION_WIDTH = 300

AUDIO_FILE_PATH = u"./staying for too long.wav"
MANUAL_BUTTONS_LOG_FILE_PATH = "./Manual_Buttons.txt"
MAXIMUM_AUDIO_PLAY_TRIES = 3

IMAGE_SAVE_PATH = "./Images/"
MAX_DATA_SEND_ATTEMPT = 3
API = '154A45'

CAPTURE_TEST_URL = 'https://dashboard.antimatter.lk/device/getdata.php?d_id=' + DEVICE_ID + '&type=get_img'
CAPTURE_SEND_URL = 'https://dashboard.antimatter.lk/device/get_img.php'

AUDIO_PLAY_TEST_URL = 'https://dashboard.antimatter.lk/device/getdata.php?d_id=' + DEVICE_ID + '&type=audio'
AUDIO_PLAY_DELETE_URL = 'https://dashboard.antimatter.lk/device/audio.php'

ENCODING = 'utf-8'

image_data_json = {'API': API,
             'd_id': DEVICE_ID,
             'get_img': 'NULL'}

audio_data_json = {'API': API,
             'd_id': DEVICE_ID}

def sendJson():
    global image_data_json
    now = datetime.now()
    current_date_time = now.strftime("(%Y-%m-%d) %H-%M-%S")
    log_message = "Sending the image @ = " + str(current_date_time)
    print(log_message)
    updateLog(log_message)
    #print(image_data_json)
    try:
        for i in range(MAX_DATA_SEND_ATTEMPT):
            request_send = requests.post(CAPTURE_SEND_URL, json=image_data_json)
            #print("Request response:", request_send.status_code)
            if (int(request_send.status_code) == 200):  # send failed
                print("Request response:", request_send.status_code)
                # print(request_send.text)
                break
            else:
                log_message = "Request response:" + str(request_send.status_code)
                print(log_message)
                updateLog(log_message)
        else:  # if break is not occured
            log_message = "Failed to send request for (times):" + str(MAX_DATA_SEND_ATTEMPT)
            print(log_message)
            updateLog(log_message)
    except:
        log_message = "Exception occurred send request_send"
        print(log_message)
        updateLog(log_message)
        pass

def updateDataJson(image_path):
    global image_data_json
    try:
        """
        with open(image_path, "rb") as image_file:
            base64_bytes = base64.b64encode(image_file.read())
            # print(base64_bytes)
            base64_string = base64_bytes.decode(ENCODING)
            image_data_json['helmet_img'] = "data:image/png;base64," + base64_string
        """
        # """
        helmet_image = imread(image_path)
        converted_image = pyrDown(helmet_image)
        _, im_arr = imencode('.png', converted_image)
        im_bytes = im_arr.tobytes()
        base64_bytes = base64.b64encode(im_bytes)
        base64_string = base64_bytes.decode(ENCODING)
        image_data_json['helmet_img'] = "data:image/png;base64," + base64_string
        # """
    except:
        print("Unable to convert to base64")

def captureAndSaveImage():
    global IMAGE_SAVE_PATH
    global image_data_json
    image_data_json['get_img'] = 'NULL'
    for idx in range(MAX_CAPTURE_ATTEMPT):
        try:
            # cam = VideoCapture(CAMERA_PORT, CAP_DSHOW)
            cam = VideoCapture(CAMERA_PORT)
            result, image = cam.read()
            if result:
                # imshow("AntiMatterRFID", image)
                now_time = datetime.now()
                current_date_time = now_time.strftime("(%Y-%m-%d) %H-%M-%S")
                # print("Current Time =", current_time)
                if not(os.path.exists(IMAGE_SAVE_PATH)):
                    os.mkdir(IMAGE_SAVE_PATH)
                png_name = current_date_time + ".png"
                log_message = 'Image name: ' + png_name
                print(log_message)
                updateLog(log_message)

                imwrite(os.path.join(IMAGE_SAVE_PATH, png_name), image)
                # waitKey(0)
                # destroyWindow("AntiMatterRFID")
                cam.release()

                converted_image = pyrDown(image)
                _, im_arr = imencode('.png', converted_image)
                im_bytes = im_arr.tobytes()
                base64_bytes = base64.b64encode(im_bytes)
                base64_string = base64_bytes.decode(ENCODING)
                image_data_json['get_img'] = "data:image/png;base64," + base64_string
                print("Captured Image")
                return True
            else:
                log_message = "No Image"
                print(log_message)
                updateLog(log_message)
        except Exception as e:
            print(e)
            print("Error occured while capturing the image")
            pass
        time.sleep(5)  # Sleep 5s before next try
    return False

def getManualCaptureStatus():
    try:
        request_send = requests.post(CAPTURE_TEST_URL)
        if (int(request_send.status_code) == 200):  # send failed
            # print("Request response:", request_send.status_code)
            # print(request_send.text)
            if(int(request_send.text) > 0):
                now = datetime.now()
                current_date_time = now.strftime("(%Y-%m-%d) %H-%M-%S")
                log_message = "Requested image @ = " + str(current_date_time)
                print(log_message)
                updateLog(log_message)
                return True
            else:
                return False
        else:
            return False
    except:
        log_message = "Exception occurred send request_send"
        print(log_message)
        updateLog(log_message)
        pass
    return False

def getPlayAudioStatus():
    try:
        request_send = requests.post(AUDIO_PLAY_TEST_URL)
        if (int(request_send.status_code) == 200):  # send failed
            # print("Request response:", request_send.status_code)
            # print(request_send.text)
            if(int(request_send.text) > 0):
                now = datetime.now()
                current_date_time = now.strftime("(%Y-%m-%d) %H-%M-%S")
                log_message = "Requested Audio Play @ = " + str(current_date_time)
                print(log_message)
                updateLog(log_message)
                return True
            else:
                return False
        else:
            return False
    except:
        log_message = "Exception occurred send request_send"
        print(log_message)
        updateLog(log_message)
        pass
    return False

def playAudio():
    for count in range(MAXIMUM_AUDIO_PLAY_TRIES):
        try:
            playsound(AUDIO_FILE_PATH)
            log_message = "Playing Staying too long Audio"
            print(log_message)
            updateLog(log_message)
            return True
        except Exception as e:
            print(e)
            log_message = "Failed audio Play !!! attempt: " + str(count)
            print(log_message)
            updateLog(log_message)
    return False

def clearAudio():
    global audio_data_json
    now = datetime.now()
    current_date_time = now.strftime("(%Y-%m-%d) %H-%M-%S")
    log_message = "Clearing Audio Play @ = " + str(current_date_time)
    print(log_message)
    updateLog(log_message)
    try:
        for i in range(MAX_DATA_SEND_ATTEMPT):
            request_send = requests.post(AUDIO_PLAY_DELETE_URL, json=audio_data_json)
            # print("Request response:", request_send.status_code)
            if (int(request_send.status_code) == 200):  # send failed
                print("Request response:", request_send.status_code)
                # print(request_send.text)
                break
            else:
                log_message = "Request response:" + str(request_send.status_code)
                print(log_message)
                updateLog(log_message)
        else:  # if break is not occured
            log_message = "Failed to send audio clear request for (times):" + str(MAX_DATA_SEND_ATTEMPT)
            print(log_message)
            updateLog(log_message)
    except:
        log_message = "Exception occurred send request_send"
        print(log_message)
        updateLog(log_message)
        pass

def updateLog(write_string):
    try:
        with open(MANUAL_BUTTONS_LOG_FILE_PATH, "a") as file_object:
            # Append write_string + \n at the end of file
            file_object.write(write_string + '\n')
    except:
        print("Failed inside updateLog Function")
        pass

while (True):
    try:
        if(getManualCaptureStatus() == True):
            if((captureAndSaveImage()) == True):
                sendJson()
        if(getPlayAudioStatus() == True):
            if(playAudio() == True):
                clearAudio()

        time.sleep(5)

    except Exception as e:
        log_message = 'main loop error: ' + str(e)
        print(log_message)
        updateLog(log_message)

