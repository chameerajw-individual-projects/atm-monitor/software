import requests
import serial
import os
import time
import json
import base64
# from cv2 import (VideoCapture, imshow, waitKey, imwrite, destroyWindow, CAP_DSHOW, CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT)
from cv2 import (VideoCapture, imread, imencode, imshow, waitKey, pyrDown, imwrite, destroyWindow, CAP_DSHOW, CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT)
from datetime import datetime
import yaml

BASE_CONF_FILE_PATH = "./../base_config.yaml"

try:
    with open(BASE_CONF_FILE_PATH, "r") as base_cfg_file:
        base_cfg = yaml.safe_load(base_cfg_file)

    COM_PORT = str(base_cfg["RFID_COM_PORT"])
    DEVICE_ID = str(base_cfg["DEVICE_ID"])

except:
    print("An exception occured at loading BASE CONFIG")


CAMERA_PORT = 0  # 1 for USB cam. 0 for Lap cam
# CAMERA_PORT = 'rtsp://admin:antimatter@123@169.254.60.62:554/Streaming/channels/1/'  # /1 for better quality
MAX_CAPTURE_ATTEMPT = 10
CAM_RESOLUTION_HEIGHT = 200
CAM_RESOLUTION_WIDTH = 300

ENCODING = 'utf-8'

POST_URL = 'https://dashboard.antimatter.lk/device/cleaning.php'
MAX_DATA_SEND_ATTEMPT = 3
API = '154A45'

IMAGE_SAVE_PATH = "./Images/"

RFID_LOG_FILE_PATH = "./RFID_log.txt"

RFID_SET_TUPLE = ("04 8F 83 D2 9F 65 80",
                  "04 D6 B9 D2 9F 65 80",
                  "04 B6 2A D2 9F 65 80",
                  "04 3F BA D2 9F 65 81",
                  "04 B5 19 D2 9F 65 80",
                  "04 81 81 D2 9F 65 80",
                  "04 EC 5B D2 9F 65 80",
                  "04 BF 84 D2 9F 65 80",
                  "04 AA 8F D2 9F 65 80",
                  "04 5A BA D2 9F 65 80",
                  "04 5A BA D2")
id_count=0
device_id_list=("NTB-0001",
                "NTB-0002",
                "NTB-0003",
                "NTB-0004",
                "NTB-0005",
                "NTB-0006",
                "NTB-0007",
                "NTB-0008",
                "NTB-0009")

data_json = {'API': API,
             'd_id': DEVICE_ID,
             'rfid_id': "00 00 00 00",
             'rec_desc': 'Start',
             'img': ''}

print("RFID READER INITIALIZING")
print("***********************************************************************************")
"""
while(True):
    try:
        serialPort = serial.Serial(COM_PORT, baudrate=115200, timeout=5)  # check every 5 sec
        break
    except Exception as e:
        print(e)
        print("FAILED TO OPEN COM-PORT:", COM_PORT)
    time.sleep(1)  # Retry to open serial port every 5s
"""
is_process_running = False

next_image_ID = "Start"

def findRfid(in_string, start, end):
    try:
        if(len(in_string) > 5):
            start_idx = in_string.index(start)
            end_idx = in_string.index(end)
            return in_string[start_idx + 1: end_idx]
        else:
            return ''
    except Exception as e:
        print(e)
        print("Failed inside findRfid function")
        return ""

def readSerail():
    try:
        global serialPort
        global id_count
        while(True):
            try:
                # read_string = str(serialPort.readline())  # convert the byte to string
                read_string = "!04 8F 83 D2 9F 65 80@"
                data_json['d_id'] = device_id_list[id_count]
                id_count = id_count+1
                time.sleep(5)
                break

            except Exception as e:
                print(e)
                log_message = "Serial Port Disconnected"
                print(log_message)
                updateLog(log_message)
                try:
                    serialPort.close()
                    time.sleep(5)
                    serialPort = serial.Serial(COM_PORT, baudrate=115200, timeout=5)  # check every 5 sec
                    time.sleep(5)  # If successfully reopened- Wait the loop for 5s
                except:
                    log_message = "Failed to Re-Open Serial Port"
                    print(log_message)
                    updateLog(log_message)
                    continue


        if(len(read_string) > 10):
            RFID_read = findRfid(str(read_string), "!", "@")
            log_message = 'received RFID ID: ' + str(RFID_read)
            print(log_message)
            updateLog(log_message)

            for ID in RFID_SET_TUPLE:
                if (RFID_read == ID):
                    data_json["rfid_id"] = RFID_read
                    return True
            log_message = "Unidentified RFID: " + RFID_read
            print(log_message)
            updateLog(log_message)
            return False
        else:
            return False
    except:
        print("Failed inside readSerial function")
        return False

""""
def captureAndSaveImage(count):
    cam = VideoCapture(cam_port)
    result, image = cam.read()
    if result:
        #imshow("AntiMatterRFID", image)
        now = datetime.now()
        now_time = datetime.now()
        current_date_time = now_time.strftime("(%Y-%m-%d) %H-%M-%S")
        png_name = "AntiMatterRFID-(" + count + ") " + current_date_time + ".png"
        print("png_name =", png_name)
        imwrite(os.path.join(IMAGE_SAVE_PATH, png_name), image)
        #waitKey(0)
        #destroyWindow("AntiMatterRFID")
        cam.release()
"""

def captureAndSaveImage(count):
    global IMAGE_SAVE_PATH
    for idx in range(MAX_CAPTURE_ATTEMPT):
        try:
            # cam = VideoCapture(CAMERA_PORT, CAP_DSHOW)
            cam = VideoCapture(CAMERA_PORT)
            # cam.set(CAP_PROP_FRAME_WIDTH, CAM_RESOLUTION_WIDTH)
            # cam.set(CAP_PROP_FRAME_HEIGHT, CAM_RESOLUTION_HEIGHT)
            result, image = cam.read()
            if result:
                # imshow("AntiMatterRFID", image)
                now_time = datetime.now()
                current_date_time = now_time.strftime("(%Y-%m-%d) %H-%M-%S")
                #print("Current Time =", current_time)
                if not(os.path.exists(IMAGE_SAVE_PATH)):
                    os.mkdir(IMAGE_SAVE_PATH)
                png_name = current_date_time + " (" + count + ")" + ".png"
                log_message = 'Image name: ' + png_name
                print(log_message)
                updateLog(log_message)

                imwrite(os.path.join(IMAGE_SAVE_PATH, png_name), image)
                # waitKey(0)
                # destroyWindow("AntiMatterRFID")
                cam.release()

                converted_image = pyrDown(image)
                _, im_arr = imencode('.png', converted_image)
                im_bytes = im_arr.tobytes()
                base64_bytes = base64.b64encode(im_bytes)
                base64_string = base64_bytes.decode(ENCODING)
                print("Captured Image")
                return "data:image/png;base64," + base64_string
                #return "NULL"
            else:
                log_message = "No Image"
                print(log_message)
                updateLog(log_message)
        except Exception as e:
            print(e)
            print("Error occured while capturing the image")
            pass
        time.sleep(5)  # Sleep 5s before next try
    return 'NULL'

def updateLog(write_string):
    try:
        with open(RFID_LOG_FILE_PATH, "a") as file_object:
            # Append write_string + \n at the end of file
            file_object.write(write_string + '\n')
    except:
        print("Failed inside updateLog Function")
        pass

def sendJson():
    global data_json
    print(data_json)
    now = datetime.now()
    current_date_time = now.strftime("(%Y-%m-%d) %H-%M-%S")
    log_message = "Sending the request @ = " + str(current_date_time)
    print(log_message)
    updateLog(log_message)
    # print(data_json)
    try:
        for i in range(MAX_DATA_SEND_ATTEMPT):
            request_send = requests.post(POST_URL, json=data_json)
            # print("Request response:", request_send.status_code)
            if (int(request_send.status_code) == 200):  # send failed
                # print("Request response:", request_send.status_code)
                print(request_send.text)
                break
            else:
                log_message = "Request response:" + str(request_send.status_code)
                print(log_message)
                updateLog(log_message)
        else:  # if break is not occured
            log_message = "Failed to send request for (times):" + str(MAX_DATA_SEND_ATTEMPT)
            print(log_message)
            updateLog(log_message)
    except:
        log_message = "Exception occurred send request_send"
        print(log_message)
        updateLog(log_message)
        pass
    log_message = "***********************************************************************************"  # Line terminator
    print(log_message)
    updateLog(log_message)

while(True):
    try:
        if(readSerail()): #start of the process. entry read
            is_process_running = True
            data_json['rec_desc'] = "Start"
            data_json['img'] = captureAndSaveImage(next_image_ID)  # save first image
            sendJson()
            next_image_ID = "3 Min"
            start_time = time.time() #start stopwatch
            while(is_process_running):
                if (readSerail()): #end of the process. exit read
                    data_json['rec_desc'] = "Start"
                    data_json['img'] = captureAndSaveImage("Start")  # save first image
                    sendJson()#save last image
                    next_image_ID = "Start"
                    is_process_running = False
                    break
                else:
                    elapsed_time = int((time.time() - start_time) / 60) #converting to minutes

                    #print("next_image_ID: ", next_image_ID)
                    #print("elapsed_time: ", elapsed_time)
                    if((next_image_ID == "3 Min") and (elapsed_time >= 3)):
                        data_json['rec_desc'] = "3 Min"
                        data_json['img'] = captureAndSaveImage(next_image_ID)  # save first image
                        sendJson()  # save 3 min image
                        next_image_ID = "6 Min"
                    elif((next_image_ID == "6 Min") and (elapsed_time >= 6)):
                        data_json['rec_desc'] = "6 Min"
                        data_json['img'] = captureAndSaveImage(next_image_ID)  # save first image
                        sendJson()  # save 6 min image
                        next_image_ID = "9 Min"
                    elif ((next_image_ID == "9 Min") and (elapsed_time >= 9)):
                        data_json['rec_desc'] = "9 Min"
                        data_json['img'] = captureAndSaveImage(next_image_ID)  # save first image
                        sendJson() # save 9 min image
                        next_image_ID = "12 Min"
                    elif ((next_image_ID == "12 Min") and (elapsed_time >= 12)):
                        data_json['rec_desc'] = "12 Min"
                        data_json['img'] = captureAndSaveImage(next_image_ID)  # save first image
                        sendJson()  # save 12 min image
                        next_image_ID = "End"
                    elif ((next_image_ID == "End") and (elapsed_time >= 15)):
                        data_json['rec_desc'] = "End"
                        data_json['img'] = captureAndSaveImage(next_image_ID)  # save first image
                        sendJson()  # save last image
                        next_image_ID = "Start"
                        is_process_running = False
                        break
                    time.sleep(2)
    except Exception as e:
        log_message = 'main loop error: ' + str(e)
        print(log_message)
        updateLog(log_message)




