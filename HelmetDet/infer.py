'''
Created on Sep 10, 2022

@author: tehara
'''

import os
import shutil
import time
import requests
import json
from json import dumps

from datetime import datetime
import base64
from cv2 import (VideoCapture, imread, imencode, imshow, waitKey, pyrDown, imwrite, destroyWindow, CAP_DSHOW, CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT)
import yaml
from playsound import playsound  # playsound==1.2.2
import numpy as np
import tensorflow as tf
from datetime import datetime
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import load_img, img_to_array
import yaml

from ultralytics import YOLO

BASE_CONF_FILE_PATH = "./../base_config.yaml"


try:
    with open(BASE_CONF_FILE_PATH, "r") as base_cfg_file:
        base_cfg = yaml.safe_load(base_cfg_file)

    DEVICE_ID = str(base_cfg["DEVICE_ID"])
    HELMET_DET_TRAIN_FILE = str(base_cfg["HELMET_DET_TRAIN_FILE"])

except:
    print("An exception occured at loading BASE CONFIG")


AUDIO_FILE_PATH = u"./helmet detected. please remove .wav"
NEW_IMAGE_PATH = "./../Sensor_reader/Images/Person_In/"
CHECKED_IMAGE_PATH = "./../Sensor_reader/Images/Person_In/Checked/"
HELMET_DET_LOG_FILE_PATH = "./Helmet_Detection_log.txt"

MAXIMUM_TRIES = 3

POST_URL = 'https://dashboard.antimatter.lk/device/helmet.php'
MAX_DATA_SEND_ATTEMPT = 3
API = '154A45'

ENCODING = 'utf-8'

data_json = {'API': API,
             'd_id': DEVICE_ID,
             'helmet_img': 'NULL'}

# Tehara's Class######################################
class HelmetDet:
    def __init__(self,
                 model_path,
                 config_file,
                 display_config=False):
        self.class_names = None
        self.im_h = None
        self.im_w = None
        self.im_c = None
        self.model = None
        self.config = None

        self.load_model(model_path)
        self.load_config(config_file, display_config)

    def load_model(self, model_path):
        self.model = load_model(model_path)

    def load_config(self, config_file, display=False):
        with open(config_file) as f:
            self.config = yaml.safe_load(f)
        if display:
            print(yaml.dump(self.config, default_flow_style=False, default_style=''))
        self.im_h, self.im_w, self.im_c = int(self.config['input']['img_h']), \
                                          int(self.config['input']['img_w']), \
                                          int(self.config['input']['img_c'])
        self.class_names = self.config['input']['class_names']

    def predict(self, image_path):
        image = img_to_array(load_img(image_path))
        image_in = np.expand_dims(self.preprocess_input(image), 0)  # (1, 224, 224, 3)
        conf = self.model.predict(image_in)
        prediction = self.class_names[np.argmax(conf[0])]
        #print(f'Prediction: {prediction} - Conf: {conf[0]}')
        return self.class_names[np.argmax(conf[0])]

    def preprocess_input(self, image):
        resize_and_rescale = tf.keras.Sequential([
            tf.keras.layers.Resizing(self.im_h, self.im_w),
            tf.keras.layers.Rescaling(1. / 255)
        ])
        return resize_and_rescale(image)
# End of class

# Kanishka's Class######################################
class Validity:

    def __init__(self, model_path="models/v4-200.pt", conf = 0.7):
        self.model = YOLO(model_path)
        self.names = list(self.model.names.values())
        print(self.model.names)

    def get_validity(self, image_path):
        objects = [0, 0, 0]
        result = self.model.predict(source=image_path)
        for r in result:
            for c in r.boxes.cls:
                objects[int(c)] = objects[int(c)] + 1
        return dict(zip(self.names, objects))

test = Validity()


def updateLog(write_string):
    try:
        with open(HELMET_DET_LOG_FILE_PATH, "a") as file_object:
            # Append write_string + \n at the end of file
            now = datetime.now()
            current_date_time = now.strftime("(%Y-%m-%d) %H-%M-%S")
            file_object.write(str(current_date_time) + ": " + write_string + '\n')
    except:
        print("Failed inside updateLog Function")
        pass

def updateDataJson(image_path):
    global data_json
    data_json['get_img'] = 'NULL'
    try:
        """
        with open(image_path, "rb") as image_file:
            base64_bytes = base64.b64encode(image_file.read())
            # print(base64_bytes)
            base64_string = base64_bytes.decode(ENCODING)
            data_json['helmet_img'] = "data:image/png;base64," + base64_string
        """
        # """
        helmet_image = imread(image_path)
        converted_image = pyrDown(helmet_image)
        _, im_arr = imencode('.png', converted_image)
        im_bytes = im_arr.tobytes()
        base64_bytes = base64.b64encode(im_bytes)
        base64_string = base64_bytes.decode(ENCODING)
        data_json['helmet_img'] = "data:image/png;base64," + base64_string
        # """
    except:
        print("Unable to convert to base64")

if __name__ == '__main__':
    _model_path = "helmet_det_" + HELMET_DET_TRAIN_FILE + ".h5"
    _config_file = 'config.yaml'
    # _image_path1 = './dataset_test/with_helmet/With_helmet (1).png'
    # _image_path2 = './dataset_test/without_helmet/Without_helmet (1).png'

    try:
        hd = HelmetDet(model_path=_model_path,
                   config_file=_config_file)
    except:
        print("Load Model failed")

    """
    for i in range(1, 17):
        image_path_with_helmet = "./dataset_test/with_helmet/With_helmet (" + str(i) + ").png"
        print(image_path_with_helmet)
        hd.predict(image_path_with_helmet)
        image_path_without_helmet = "./dataset_test/without_helmet/Without_helmet (" + str(i) + ").png"
        print(image_path_without_helmet)
        hd.predict(image_path_without_helmet)
        print("**************************************************************************")
    """
    savedSet = set()

    try:
        for file in os.listdir(NEW_IMAGE_PATH):
            fullpath = os.path.join(NEW_IMAGE_PATH, file)
            shutil.move(fullpath, CHECKED_IMAGE_PATH)

        # After moving Files, check and update initial list
        for file in os.listdir(NEW_IMAGE_PATH):
            fullpath = os.path.join(NEW_IMAGE_PATH, file)
            if os.path.isfile(fullpath):
                savedSet.add(file)
        log_message = "Initial File count: " + str(len(savedSet))
        print(log_message)
        updateLog(log_message)

        log_message = "********************************************************************"
        print(log_message)
        updateLog(log_message)

    except Exception as e:
        log_message = "Initial file listing fail: " + str(e)
        print(log_message)
        updateLog(log_message)

    while True:
        try:
            retrievedSet = set()
            for file in os.listdir(NEW_IMAGE_PATH):
                fullpath = os.path.join(NEW_IMAGE_PATH, file)
                if os.path.isfile(fullpath):
                    retrievedSet.add(file)

            newSet = retrievedSet - savedSet
            if(len(newSet) > 0):
                for newImage in newSet:
                    try:
                        image_path_full = os.path.join(NEW_IMAGE_PATH, newImage)
                        log_message = "New File detected: " + str(newImage)
                        print(log_message)
                        updateLog(log_message)

                        time.sleep(1)  # wait for the image to be saved
                        received_prediction = ""
                        for count in range(MAXIMUM_TRIES):
                            try:
                                # received_prediction = hd.predict(image_path_full)
                                result = test.get_validity(image_path_full)
                                try:
                                    print(str(result))
                                    if (int(result["Helmet"]) > 0):
                                        received_prediction = "With Helmet"
                                    else:
                                        received_prediction = "Without Helmet"
                                except:
                                    log_message = "Failed to convert to int"
                                    print(log_message)
                                    updateLog(log_message)

                                log_message = "Prediction: " + str(received_prediction)
                                print(log_message)
                                updateLog(log_message)
                                break  # if successful, break
                            except Exception as e:
                                print(e)
                                log_message = "Failed Prediction !!! attempt: " + str(count)
                                print(log_message)
                                updateLog(log_message)
                            time.sleep(2)

                        if (received_prediction == "With Helmet"):
                            for count in range(MAXIMUM_TRIES):
                                try:
                                    playsound(AUDIO_FILE_PATH)
                                    log_message = "Playing Helmet Detected Audio"
                                    print(log_message)
                                    updateLog(log_message)
                                    break  # if successful, break
                                except Exception as e:
                                    print(e)
                                    log_message = "Failed audio Play !!! attempt: " + str(count)
                                    print(log_message)
                                    updateLog(log_message)

                            try:
                                updateDataJson(image_path_full)
                                for i in range(MAX_DATA_SEND_ATTEMPT):
                                    # print(data_json)
                                    request_send = requests.post(POST_URL, json=data_json)
                                    if (int(request_send.status_code) == 200):  # send failed
                                        # print("Request response:", request_send.status_code)
                                        # print(request_send.text)
                                        break
                                    else:
                                        log_message = "Request response:" + str(request_send.status_code)
                                        print(log_message)
                                        updateLog(log_message)
                                else:  # if break is not occured
                                    log_message = "Failed to send request for (times):" + str(MAX_DATA_SEND_ATTEMPT)
                                    print(log_message)
                                    updateLog(log_message)
                            except:
                                log_message = "Exception occurred send request_send"
                                print(log_message)
                                updateLog(log_message)
                                pass                     

                        try:
                            shutil.move(image_path_full, CHECKED_IMAGE_PATH)  # Move predicted image
                        except:
                            pass

                    except Exception as e:
                        log_message = e
                        print(log_message)
                        updateLog(log_message)

                    log_message = "********************************************************************"
                    print(log_message)
                    updateLog(log_message)

                savedSet = retrievedSet

            deleteSet = savedSet - retrievedSet
            if (len(deleteSet) > 0):
                """"
                log_message = "File REMOVED: " + str(len(deleteSet))
                print(log_message)
                updateLog(log_message)

                log_message = "********************************************************************"
                print(log_message)
                updateLog(log_message)
                """
                savedSet = retrievedSet

        except Exception as e:
            log_message = "While loop fail: " + str(e)
            print(log_message)
            updateLog(log_message)


