'''
Created on Sep 10, 2022

@author: teharaf
'''
import numpy as np
import yaml
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.layers import AveragePooling2D, Dropout, Flatten, Dense, Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import image_dataset_from_directory


class HelmetDetTrainer:
    def __init__(self,
                 config_file,
                 display_config=False):
        self.cfg = None
        self.im_h, self.im_w, self.im_c = None, None, None
        self.bs = None
        self.init_lr = None
        self.epochs = None
        self.trained_model_path = None
        self.data_dir = None
        self.step_decay = None
        self.train_history = None
        self.model = None
        self.train_ds = None
        self.test_data_dir = None
        self.val_ds = None
        self.test_ds = None
        self.class_names = None

        self.load_config(config_file, display_config)

    def load_config(self, config_file, display=False):
        with open(config_file) as f:
            self.cfg = yaml.safe_load(f)
        if display:
            print(yaml.dump(self.cfg, default_flow_style=False, default_style=''))

        # Load Config
        self.im_h, self.im_w, self.im_c = int(self.cfg['input']['img_h']), \
                                          int(self.cfg['input']['img_w']), \
                                          int(self.cfg['input']['img_c'])
        self.bs = int(self.cfg['hyperparams']['bs'])
        self.init_lr = float(self.cfg['hyperparams']['init_lr'])
        self.epochs = int(self.cfg['hyperparams']['epochs'])
        self.trained_model_path = self.cfg['model_save_path']
        self.data_dir = self.cfg['dataset_path']
        self.test_data_dir = self.cfg['test_dataset_path']
        self.step_decay = self.init_lr / self.epochs
        self.class_names = self.cfg['input']['class_names']

    def load_data(self):
        """
        main_directory/
            ...class_a/
            ......a_image_1.jpg
            ......a_image_2.jpg
            ...class_b/
            ......b_image_1.jpg
            ......b_image_2.jpg
        """
        train_ds = image_dataset_from_directory(
            self.data_dir,
            validation_split=0.2,
            subset="training",
            seed=1,
            image_size=(self.im_h, self.im_w),
            batch_size=self.bs,
            label_mode='categorical')
        train_ds = self.prepare_data(train_ds, shuffle=True, augment=True)

        val_ds = image_dataset_from_directory(
            self.data_dir,
            validation_split=0.2,
            subset="validation",
            seed=1,
            image_size=(self.im_h, self.im_w),
            batch_size=self.bs,
            label_mode='categorical')
        val_ds = self.prepare_data(val_ds, shuffle=False, augment=False)

        test_ds = image_dataset_from_directory(
            self.test_data_dir,
            seed=1,
            image_size=(self.im_h, self.im_w),
            batch_size=self.bs,
            label_mode='categorical')
        test_ds = self.prepare_data(test_ds, shuffle=False, augment=False)

        self.visualize_data(train_ds, self.class_names)
        print('class_names:', self.class_names)

        return train_ds, val_ds, test_ds

    @staticmethod
    def visualize_data(train_ds, class_names):
        plt.figure(figsize=(10, 10))
        for images, labels in train_ds.take(1):
            print(f'Batched images shape: {images.shape} -- labels.shape: {labels.shape}')
            for i in range(len(images)):
                ax = plt.subplot(len(images) // 2, 2, i + 1)
                ax.imshow((images[i] * 255).numpy().astype("uint8"))
                plt.title(class_names[np.argmax(labels[i])])
                plt.axis("off")
        plt.show()

    def prepare_data(self, ds, shuffle=False, augment=False):
        resize_and_rescale = tf.keras.Sequential([
            tf.keras.layers.Resizing(self.im_h, self.im_w),
            tf.keras.layers.Rescaling(1. / 255)
        ])

        data_augmentation = tf.keras.Sequential([
            tf.keras.layers.RandomFlip("horizontal"),
            tf.keras.layers.RandomRotation(0.1, fill_mode='constant')
        ])
        AUTOTUNE = tf.data.AUTOTUNE
        # Resize and rescale all datasets.
        ds = ds.map(lambda x, y: (resize_and_rescale(x), y),
                    num_parallel_calls=AUTOTUNE)
        ds = ds.repeat(self.epochs)

        if shuffle:
            ds = ds.shuffle(1000)

        # Use data augmentation only on the training set.
        if augment:
            ds = ds.map(lambda x, y: (data_augmentation(x, training=True), y),
                        num_parallel_calls=AUTOTUNE)

        # Use buffered prefetching on all datasets.
        return ds.prefetch(buffer_size=AUTOTUNE)

    def build_model(self):
        base_model = MobileNetV2(weights="imagenet", include_top=False,
                                 input_tensor=Input(shape=(self.im_h, self.im_w, self.im_c)))
        for layer in base_model.layers:
            layer.trainable = False

        head_model = base_model.output
        head_model = AveragePooling2D(pool_size=(7, 7))(head_model)
        head_model = Flatten(name="flatten")(head_model)
        head_model = Dense(128, activation="relu")(head_model)
        head_model = Dropout(0.5)(head_model)
        head_model = Dense(2, activation="softmax")(head_model)

        model = Model(inputs=base_model.input, outputs=head_model)
        print(model.summary())
        return model

    def visualize(self):
        # plot the training loss and accuracy
        n = len(list(self.train_history.history.values())[0])
        plt.style.use("ggplot")
        plt.figure()
        try:
            plt.plot(np.arange(0, n), self.train_history.history["loss"], label="train_loss")
            plt.plot(np.arange(0, n), self.train_history.history["val_loss"], label="val_loss")
            plt.plot(np.arange(0, n), self.train_history.history["accuracy"], label="train_acc")
            plt.plot(np.arange(0, n), self.train_history.history["val_accuracy"], label="val_acc")
        except KeyError as e:
            print(e)
        plt.title("Training Loss and Accuracy")
        plt.xlabel("Epoch #")
        plt.ylabel("Loss/Accuracy")
        plt.legend(loc="lower left")
        plt.savefig('train_output.jpg')

    def train(self):
        # Build Model
        self.model = self.build_model()
        print('[INFO] Built Model!')

        # Initialize Optimizer
        opt = Adam(learning_rate=self.init_lr,
                   decay=self.step_decay)

        # Compile Model
        self.model.compile(loss="binary_crossentropy",
                           optimizer=opt,
                           metrics=["accuracy"])
        print('[INFO] Model Compiled!')

        # Create data generators
        self.train_ds, self.val_ds, self.test_ds = self.load_data()

        print('[INFO] Created Datasets!')

        # train the head of the network
        print("[INFO] training head...")
        self.train_history = self.model.fit(
            self.train_ds,
            steps_per_epoch=len(self.train_ds) // (self.bs*self.epochs),
            validation_data=self.val_ds,
            validation_steps=len(self.val_ds) // (self.bs*self.epochs),
            epochs=self.epochs,
            verbose=1)

        self.visualize()

        # save the trained model
        print("[INFO] saving mask detector model...")
        self.model.save(self.trained_model_path, save_format="h5")

    def evaluate(self):
        acc = self.model.evaluate(self.test_ds, verbose=0)[1]
        print("Accuracy: {:.2f}%".format(acc * 100))


if __name__ == '__main__':
    cfg_path = 'config.yaml'
    trainer = HelmetDetTrainer(cfg_path)
    trainer.train()
    trainer.evaluate()
