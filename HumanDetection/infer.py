'''
Created on Sep 10, 2022

@author: tehara
'''

import os
import shutil
import time

from cv2 import (VideoCapture, imread, imencode, imshow, waitKey, pyrDown, imwrite, destroyWindow, CAP_DSHOW, CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT)
import numpy as np
import tensorflow as tf
from datetime import datetime
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import load_img, img_to_array
import yaml

HELMET_DET_LOG_FILE_PATH = "./Helmet_Detection_log.txt"

CAMERA_PORT = 'rtsp://admin:antimatter@123@169.254.60.62:554/Streaming/channels/1/'  # /1 for better quality
#CAMERA_PORT = 0
MAX_CAPTURE_ATTEMPT = 10
CAM_RESOLUTION_HEIGHT = 200
CAM_RESOLUTION_WIDTH = 300

MAX_ALLOWED_TIME = 1.0   # in minutes
CHECKING_PERIOD = 5  # checking frequency in seconds

IMAGE_SAVE_PATH = "./../Sensor_reader/Images/Overtime/"

class HumanDet:
    def __init__(self,
                 model_path,
                 config_file,
                 display_config=False):
        self.class_names = None
        self.im_h = None
        self.im_w = None
        self.im_c = None
        self.model = None
        self.config = None

        self.load_model(model_path)
        self.load_config(config_file, display_config)

    def load_model(self, model_path):
        self.model = load_model(model_path)

    def load_config(self, config_file, display=False):
        with open(config_file) as f:
            self.config = yaml.safe_load(f)
        if display:
            print(yaml.dump(self.config, default_flow_style=False, default_style=''))
        self.im_h, self.im_w, self.im_c = int(self.config['input']['img_h']), \
                                          int(self.config['input']['img_w']), \
                                          int(self.config['input']['img_c'])
        self.class_names = self.config['input']['class_names']

    def predict(self, image):
        image_in = np.expand_dims(self.preprocess_input(image), 0)  # (1, 224, 224, 3)
        conf = self.model.predict(image_in)
        prediction = self.class_names[np.argmax(conf[0])]
        #print(f'Prediction: {prediction} - Conf: {conf[0]}')
        return self.class_names[np.argmax(conf[0])]

    def preprocess_input(self, image):
        resize_and_rescale = tf.keras.Sequential([
            tf.keras.layers.Resizing(self.im_h, self.im_w),
            tf.keras.layers.Rescaling(1. / 255)
        ])
        return resize_and_rescale(image)
# End of class

def updateLog(write_string):
    try:
        with open(HELMET_DET_LOG_FILE_PATH, "a") as file_object:
            # Append write_string + \n at the end of file
            now = datetime.now()
            current_date_time = now.strftime("(%Y-%m-%d) %H-%M-%S")
            file_object.write(str(current_date_time) + ": " + write_string + '\n')
    except:
        print("Failed inside updateLog Function")
        pass
captured_image = 0
def captureAndDetectHuman():
    global captured_image
    for idx in range(MAX_CAPTURE_ATTEMPT):
        try:
            # cam = VideoCapture(CAMERA_PORT, CAP_DSHOW)
            cam = VideoCapture(CAMERA_PORT)
            result, image = cam.read()
            if result:
                cam.release()
                received_prediction = hd.predict(image)
                print(received_prediction)
                if (received_prediction == "With Human"):
                    captured_image = image
                    return True
                else:
                    return False
            else:
                log_message = "No Image"
                print(log_message)
                updateLog(log_message)
        except Exception as e:
            print(e)
            print("Error occured while capturing the image")
            pass
        time.sleep(5)  # Sleep 5s before next try
    return False

def saveImage():
    global captured_image
    try:
        current_date_time = now_time.strftime("(%Y-%m-%d) %H-%M-%S")
        # print("Current Time =", current_time)
        if not (os.path.exists(IMAGE_SAVE_PATH)):
            os.mkdir(IMAGE_SAVE_PATH)
        png_name = current_date_time + ".png"
        log_message = 'Image name: ' + png_name
        print(log_message)
        updateLog(log_message)

        imwrite(os.path.join(IMAGE_SAVE_PATH, png_name), captured_image)
    except Exception as e:
        log_message = "saveImage Failed: " + str(e)
        print(log_message)
        updateLog(log_message)

if __name__ == '__main__':
    _model_path = "human_det.h5"
    _config_file = 'config.yaml'
    # _image_path1 = './dataset_test/with_human/With_human (1).png'
    # _image_path2 = './dataset_test/without_human/Without_human (1).png'

    try:
        hd = HumanDet(model_path=_model_path,
                   config_file=_config_file)
    except:
        print("Load Model failed")

    is_human_active = False
    is_notified = False
    human_activity_start_time = datetime.now()
    while True:
        try:
            if(is_human_active):
                print("human_active")
                if not(captureAndDetectHuman()):  # Human is gone
                    print("human_gone")
                    is_human_active = False
                    is_notified = False
                else:
                    now_time = datetime.now()
                    time_diff = ((now_time - human_activity_start_time).total_seconds()) / 60.0
                    if(time_diff >= MAX_ALLOWED_TIME):  # overstaying detected
                        print("exceed_active")
                        if not(is_notified):
                            saveImage()
                            print(time_diff)
                            is_notified = True
            else:
                if(captureAndDetectHuman()):
                    print("new_human_active")
                    is_human_active = True
                    is_notified = False
                    human_activity_start_time = datetime.now()

            time.sleep(CHECKING_PERIOD)

        except Exception as e:
            log_message = "While loop fail: " + str(e)
            print(log_message)
            updateLog(log_message)


